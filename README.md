# Mixed Integer Neural Inverse Design

This repository contains code for the following paper:

+ [Mixed Integer Neural Inverse Design (arxiv)](https://arxiv.org/pdf/2109.12888.pdf)

## Requirements
+ YALMIP Version 31-March-2021
+ Gurobi v 9.01
+ NumPy
+ SciPy
+ PyTorch v 1.7.1

## Instructions
For the matlab code, install `Gurobi` and `Yalmip`, and add yalmip to the matlab path.
Run `main.m` and follow the given instuctions to run each experiment or plot the results.

For the python code, install `PyTorch`, `NumPy`, and `SciPy` then for each experiment there is a `main.py` and `config.py` that you can run `main.py` and set the hyperparameters in the `config.py`.


## Code Structure

The code for each experiment is in a separate directory. Each directory corresponds to the experiments as follows:

+ `Spectral Separation`: Neural Spectral Separation from section 4.1
+ `Softrobot`: Robot Inverse Kinematic from section 4.2
+ `Inversion_and_selection`: Material Selection from section 4.3
+ `Nano-Photonics`: Nano-Photonics from section 4.4.1
+ `Contoning`: Contoning from section 4.4.2
+ `MILP_NA_combination`: Combination of MILP and NA from section 4.5
+ `Robustness`: Robustness Analysis from section 4.6

## Upcoming Update
Currently the optimization part is performed via Matlab. We are working on a solution to run everything on python.

